import jwt
from datetime import datetime, timedelta
import time


class Auth():
    def __init__(self):
        self.secret = "secret"

    def make_jwt(self, auth_information_user_id):
        today = datetime.utcnow()
        exp = today + timedelta(minutes=60) # トークンの有効期限は1時間
        token = jwt.encode({"sub": auth_information_user_id,
                            'exp': exp}, self.secret, algorithm="HS256")
        
        return token.decode("utf-8")

    def check_jwt(self, str_jwt):
        """
        Parameters
        ----------------
        str_jwt str
            string json web token

        Returns
        ----------------
        parse_token dict or None
        """
        try:
            parse_token = jwt.decode(str_jwt, key=self.secret)
            return parse_token
        except:
            parse_token = None
            return parse_token

    def auth_header_check(self, auth_header):
        parse_token = {}
        if auth_header == None:
            parse_token["login"] = False
            return parse_token
        split_list = auth_header.split()

        if len(split_list) == 2:
            _, token = split_list
            ckeck_token = self.check_jwt(token)
            if ckeck_token == None:
                parse_token["login"] = False
            else:
                parse_token = ckeck_token
                parse_token["login"] = True
        else:
            parse_token["login"] = False
        return parse_token
#  a = Auth()
# token = a.make_jwt("fukuda@gmail.com")
# print(token)
# time.sleep(1)
# print(a.check_jwt(token))
