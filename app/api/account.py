import hashlib
from sqlalchemy import exc
from datastore.models import *
from datastore.setting import DBsession
import jwt
from datetime import datetime, timedelta
import time


class Account():
    def __init__(self):
        self.hash_password = None
        self.str_email = ""
        self.password_1 = ""
        self.password_2 = ""
        self.password = ""
        self.first_name = ""
        self.last_name = ""
        self.secret = "test"
        self.auth_header = ""
        self.isSignin = False
        self.payload = {}

    def password_to_hash_password(self, str_password):
        str_password = str_password + "test"
        self.hash_password = hashlib.sha256(
            str_password.encode()).hexdigest()
        return self.hash_password

    def make_hash_password(self):
        h_pass = self.password + "test"
        self.hash_password = hashlib.sha256(
            h_pass.encode()).hexdigest()

    def make_jwt(self, auth_information_user_id):
        today = datetime.utcnow()
        exp = today + timedelta(minutes=60)  # トークンの有効期限は1時間
        token = jwt.encode({"sub": auth_information_user_id,
                            'exp': exp}, self.secret, algorithm="HS256")
        return token.decode("utf-8")

    def check_jwt(self, str_jwt):
        try:
            parse_token = jwt.decode(str_jwt, key=self.secret)
            return parse_token
        except:
            parse_token = None
            return parse_token

    def auth_header_check(self):
        auth_header = self.get_auth_header()
        if auth_header is not None:
            split_list = auth_header.split()
            if len(split_list) == 2:
                _, payload = split_list
                ckeck_token = self.check_jwt(payload)
                if ckeck_token is not None:
                    parse_token = ckeck_token
                    self.payload = parse_token
                    # print(self.payload)
                    self.isSignin = True
        return self.isSignin

    def get_hash_password(self):
        return self.hash_password

    def validation_password(self):
        if self.password_1 == self.password_2 and len(self.password_1) > 8:
            self.password = self.password_1
            return True
        else:
            return False

    def validation_email(self):
        if len(self.str_email) > 1:
            return True
        else:
            return False

    def validation_fullname(self):
        if (self.first_name and self.last_name):
            return True
        else:
            return False

    def email_check(self):
        session = DBsession()
        # 存在チェック,なければFlase
        check = session.query(AuthInformationModel.id).filter(
            AuthInformationModel.email == self.str_email).scalar() is not None
        session.close()
        return check

    def validation_user_data(self, str_email, password_1, password_2):
        len_str_email = len(str_email) > 1
        password_match = password_1 == password_2
        password_len = len(password_1) >= 8
        if len_str_email and password_match and password_len:
            return True
        else:
            return False

    def get_id_to_name(self):
        if self.payload:
            try:
                session = DBsession()
                # 存在チェック,なければFlase
                full_name = session.query(UsersDetailsModel.first_name, UsersDetailsModel.last_name).filter(
                    UsersDetailsModel.user_id == self.payload["sub"]).all()[0]
                self.first_name = full_name[0]
                self.last_name = full_name[1]
            finally:
                session.close()
            return self.payload["sub"]

    def set_email(self, email):
        self.str_email = email

    def get_email(self):
        return self.str_email

    def set_password_1(self, password_1):
        self.password_1 = password_1

    def set_password_2(self, password_2):
        self.password_2 = password_2

    def set_password(self, password):  # Sigin Inするときにしか使わないはず
        self.password = password

    def get_password(self):
        return self.password

    def set_first_name(self, first_name):
        self.first_name = first_name

    def get_first_name(self):
        return self.first_name

    def set_last_name(self, last_name):
        self.last_name = last_name

    def get_last_name(self):
        return self.last_name

    def set_auth_header(self, auth_header):
        self.auth_header = auth_header

    def get_auth_header(self):
        return self.auth_header

    def get_jwt_auth_id(self):
        return self.payload["sub"]

    def SignUp(self):
        self.msg = {}
        if self.validation_password() and self.validation_email() and self.validation_fullname():
            if self.email_check() == True:
                # emailが存在した場合
                msg = {"msg": "email_exists", "SignUp": False}
            else:
                self.make_hash_password()
                try:
                    session = DBsession()
                    users_table = UsersModel()
                    session.add(users_table)
                    session.commit()

                    auth_information_table = AuthInformationModel()
                    auth_information_table.user_id = users_table.id
                    auth_information_table.email = self.str_email
                    auth_information_table.password = self.hash_password
                    auth_information_table.active = False
                    session.add(auth_information_table)
                    session.commit()

                    users_details_table = UsersDetailsModel()
                    users_details_table.user_id = users_table.id
                    users_details_table.first_name = self.first_name
                    users_details_table.last_name = self.last_name
                    users_details_table.email = self.str_email
                    session.add(users_details_table)
                    session.commit()
                    msg = {"msg": "ok", "SignUp": True}
                except:
                    session.rollback()
                    msg = {"msg": "SignUp Error", "SignUp": False}
                finally:
                    session.close()
        else:
            msg = {"msg": "validation err", "SignUp": False}
        return msg

    def SigIn(self):
        data = {}
        self.make_hash_password()

        session = DBsession()
        self.make_hash_password()

        try:
            auth_information_user_id = session.query(AuthInformationModel.user_id).filter(
                AuthInformationModel.email == self.get_email(), AuthInformationModel.password == self.get_hash_password()).all()
        except:
            auth_information_user_id = []
        finally:
            session.close()
        if not auth_information_user_id:
            # リストが空だったとき
            data["token"] = None
        else:
            data["token"] = self.make_jwt(auth_information_user_id[0][0])
        return data

# account = Account()
# account.set_email("4test_q@gmail.com")
# account.set_password_1("password1234")
# account.set_password_2("password1234")
# account.set_first_name("orangete")
# account.set_last_name("ryu")
# print(account.SignUp())


# account = Account()
# account.set_email("fukud_ryusei@example.com")
# account.set_password("test_test")
# print(account.SigIn())


# Beare = "Beare "
# token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI4LCJleHAiOjE1NjQ3NTE3Nzl9.2jMTOfSy045oxEyEJMAzySwtEGPFkA_zaMAo2OrLYSI"
# account = Account()
# account.set_auth_header(Beare+token)
# account.auth_header_check()
# account.get_id_to_name()
# print(account.get_first_name())
# print(account.get_last_name())
