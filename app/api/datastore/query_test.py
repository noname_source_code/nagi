# セッション変数の取得

try:
    from datastore.setting import DBsession
    from datastore.models import *
except:
    from setting import DBsession
    from models import *
# # DBにレコードの追加
# test_table = TestModel()
# test_table.theme = "test1"
# test_table.exam = "test"

# session = DBsession()
# session.add(test_table)
# session.commit()

# users = session.query(User).all()
# for user in users:
#     print(user.name)
data = {'code_test_table_id': 132, 'result': '2\n'}
session = DBsession()
s = session.query(CodeTestsModel).filter(
    CodeTestsModel.id == data["code_test_table_id"]).first()
s.result = "test"
session.commit()
