from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import *
import os


DBHOST = os.environ["DBHOST"]
DBPORT = os.environ["DBPORT"]
DATABASE = "postgresql://dev:dev@{0}:{1}/coder".format(DBHOST, DBPORT)

ENGINE = create_engine(
    DATABASE,
    encoding="utf-8",
    echo=False  # 実行のたびにSQLを出力する
)

# Sessionの作成
DBsession = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=ENGINE
    )
)


# modelで使用する
Base = declarative_base()
Base.query = DBsession.query_property()
