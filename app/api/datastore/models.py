import sys
from datetime import datetime

from sqlalchemy import (Boolean, Column, DateTime, ForeignKey, Integer,
                        Sequence, String, Text, VARCHAR)
from sqlalchemy.orm import relationship

try:
    from datastore.setting import ENGINE, Base
except:
    from setting import ENGINE, Base, DBsession


class TestModel(Base):
    """
    TestModel
    """
    __tablename__ = 'test_table'
    id = Column(Integer, primary_key=True)  # primary_keyはデフォルトで連番で作成される
    user_id = Column(Integer, nullable=False)
    theme = Column(Text, nullable=False)
    exam = Column(Text, nullable=False)
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    test_cases_table = relationship(
        'TestCasesModel', back_populates='test_table')


class TestCasesModel(Base):
    """
    TestCasesModel
    """
    __tablename__ = 'test_cases_table'
    id = Column(Integer, primary_key=True)  # primary_keyはデフォルトで連番で作成される
    test_id = Column(Integer, ForeignKey('test_table.id'), nullable=False)
    argument = Column(Text, nullable=False)
    solution = Column(Text, nullable=False)
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    test_table = relationship('TestModel', back_populates='test_cases_table')


class CodeModel(Base):
    """
    CodeModel
    """
    __tablename__ = 'code_table'
    id = Column(Integer, primary_key=True)  # primary_keyはデフォルトで連番で作成される
    test_id = Column(Integer, nullable=False)  # 外部キーの制約に依存しない
    user_id = Column(Integer, nullable=False)
    code = Column(Text, nullable=False)
    lang = Column(Text, nullable=False)
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    code_tests_table = relationship(
        'CodeTestsModel', back_populates='code_table')


class CodeTestsModel(Base):
    """
    CodeTestsModel
    """
    __tablename__ = 'code_tests_table'
    id = Column(Integer, primary_key=True)  # primary_keyはデフォルトで連番で作成される
    code_id = Column(Integer, ForeignKey('code_table.id'), nullable=False)
    test_id = Column(Integer, nullable=False)  # 外部キーの制約に依存しない
    test_cases_id = Column(Integer, nullable=False)
    result = Column(Text)
    judgment = Column(Boolean)
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    code_table = relationship('CodeModel', back_populates='code_tests_table')


class UsersModel(Base):
    """
    UsersModel
    """
    __tablename__ = 'users_table'
    id = Column(Integer, primary_key=True)  # primary_keyはデフォルトで連番で作成される
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    auth_information_table = relationship(
        'AuthInformationModel', back_populates='users_table')

    users_details_table = relationship(
        'UsersDetailsModel', back_populates='users_table')


class AuthInformationModel(Base):
    """
    AuthInformationModel
    """
    __tablename__ = 'auth_information_table'
    id = Column(Integer, primary_key=True)  # primary_keyはデフォルトで連番で作成される
    user_id = Column(Integer, ForeignKey('users_table.id'),
                     nullable=False)
    email = Column(VARCHAR(255), unique=True, nullable=False)
    password = Column(VARCHAR(255), nullable=False)
    active = Column(Boolean, nullable=False)
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    users_table = relationship(
        'UsersModel', back_populates='auth_information_table')


class UsersDetailsModel(Base):
    """
    UsersDetailsModel
    """
    __tablename__ = 'users_details_table'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users_table.id'),
                     nullable=False)  # primary_keyはデフォルトで連番で作成される
    first_name = Column(VARCHAR(255), nullable=False)
    last_name = Column(VARCHAR(255), nullable=False)
    email = Column(VARCHAR(255), unique=True, nullable=False)
    created_at = Column('created', DateTime,
                        default=datetime.now, nullable=False)
    updated_at = Column('modified', DateTime,
                        default=datetime.now, nullable=False)

    users_table = relationship(
        'UsersModel', back_populates='users_details_table')


def main(args):
    Base.metadata.create_all(bind=ENGINE)


if __name__ == "__main__":
    main(sys.argv)
    # session = DBsession()
    # test_table = CodeModel()
    # test_table.theme = "test"
    # test_table.exam = "test"
    # session.add(test_table)
    # session.commit()
