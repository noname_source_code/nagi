from datastore.models import *
from datastore.setting import DBsession
from problem import Problem


class Coder():
    def __init__(self):
        self.result = None
        self.code_id = None
        self.user_id = None
        self.code = None
        self.problem_id = None
        self.lang = None
        self.code_test_table_id = None
        self.solution = None
        self.judgment = False
        self.check_result = False
        self.choice_problem = []
        self.test_cases = []
        self.code_each_test_cases = []

    def set_choice_problem(self, choice_problem):
        self.choice_problem = choice_problem

    def get_formated_problem(self):
        if not self.choice_problem:
            # 空の場合
            return self.test_cases
        else:
            for i in self.choice_problem:
                self.test_cases.append(
                    {"test_case_id": i[0], "argument": i[1], "solution": i[2]})
            return self.test_cases

    def set_user_id(self, code_user_id):
        self.user_id = code_user_id

    def get_user_id(self):
        return self.user_id

    def set_code(self, code):
        self.code = code

    def get_code(self):
        return self.code

    def set_problem_id(self, problem_id):
        self.problem_id = problem_id

    def get_problem_id(self):
        return self.problem_id

    def set_lang(self, lang):
        self.lang = lang

    def set_results(self, result):
        self.result = result

    def get_result(self):
        return self.result

    def get_lang(self):
        return self.lang

    def get_code_id(self):
        return self.code_id

    def get_code_each_test_cases(self):
        return self.code_each_test_cases

    def set_code_test_table_id(self, code_test_table_id):
        self.code_test_table_id = code_test_table_id

    def get_code_test_table_id(self):
        return self.code_test_table_id

    def get_solution(self):
        # 想定解が入っている
        return self.solution

    def set_judgment(self, judgment):
        self.judgment = judgment

    def get_judgment(self):
        return self.judgment

    def check_solution(self):
        if self.get_solution():
            judgment = self.get_judgment()
            print(self.check_result)
            try:
                session = DBsession()
                code_test_table = session.query(CodeTestsModel).filter(
                    CodeTestsModel.id == self.get_code_test_table_id()).first()
                code_test_table.result = self.get_result()
                code_test_table.judgment = judgment
                session.commit()
            except BaseException as e:
                print(e)
                session.rollback()
            finally:
                session.close()

    def registration_code(self):
        try:
            session = DBsession()
            code_table = CodeModel()
            code_table.user_id = self.get_user_id()
            code_table.test_id = self.get_problem_id()
            code_table.code = self.get_code()
            code_table.lang = self.get_lang()
            session.add(code_table)
            session.commit()
            self.code_id = code_table.id
        except BaseException as e:
            print(e)
            session.rollback()
        finally:
            session.close()

    def registration_test_cases(self):
        problem_list = self.get_formated_problem()
        for i in problem_list:
            print(i["solution"])
            try:
                session = DBsession()
                code_tests_table = CodeTestsModel()
                code_tests_table.code_id = self.get_code_id()
                code_tests_table.test_id = self.get_problem_id()
                code_tests_table.test_cases_id = i["test_case_id"]
                session.add(code_tests_table)
                session.commit()
                self.code_each_test_cases.append(
                    (code_tests_table.id, i["argument"], i["solution"]))
            except BaseException as e:
                print(e)
                session.rollback()
            finally:
                session.close()

    def get_code_test_id_to_test_case(self):
        # code tableのidから想定解を取得する
        # {'code_test_table_id': 353, 'result': '1\n'}
        code_test_table_id = self.get_code_test_table_id()
        try:
            session = DBsession()
            test_case_id = session.query(CodeTestsModel.test_cases_id).filter(
                CodeTestsModel.id == code_test_table_id).all()[0][0]
            self.solution = session.query(TestCasesModel.solution).filter(
                TestCasesModel.id == test_case_id).all()[0][0]
        finally:
            session.close()


# coder = Coder()
# coder.set_problem_id(26)
# coder.set_user_id(1)
# coder.set_code("print(1)")
# coder.set_lang("python")
# coder.registration_code()

# problem = Problem()
# problem.set_problem_id(26)
# print(problem.choice_problem())

# coder.set_choice_problem(problem.choice_problem())
# coder.registration_test_cases()
# session = DBsession()
# code_table = CodeModel()
# code_table.code = data["code"]
# code_table.test_id = data["problemid"]
# code_table.lang = data["lang"]
# session.add(code_table)
# session.commit()
# a = [(16, 'こんにちは'), (17, 'KONITIHA')]

# coder = Coder()
# coder.set_choice_problem(a)
# print(coder.get_formated_problem())

# coder = Coder()
# coder.set_problem_id(15)
# coder.set_user_id(1)
# coder.set_code("print('test_code_user_2')")
# coder.set_lang("python")
# coder.registration_code()

# problem = Problem()
# test_cases = problem.choice_problem(15)
# coder.set_choice_problem(test_cases)
# coder.registration_test_cases()
# li = coder.get_code_each_test_cases()
# for i in li:
#     print(i)


# coder = Coder()
# coder.set_code_test_table_id(10)
# coder.get_code_test_id_to_test_case()
# coder.set_results("こんにちは")
# coder.check_solution()
