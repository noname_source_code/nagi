from datastore.models import *
from datastore.setting import DBsession


class Problem():
    def __init__(self):
        self.problem_user_id = ""
        self.submission_code = ""
        self.problem_id = None
        self.lang = ""
        self.problem_theme = ""
        self.problem_exam = ""
        self.problem_argument = ""
        self.problem_solution = ""
        self.problem_test_cases_list = []

    def set_code(self, submission_code):
        self.submission_code = submission_code

    def get_code(self):
        return self.submission_code

    def set_problem_id(self, problem_id):
        self.problem_id = problem_id

    def get_problem_id(self):
        return self.problem_id

    def set_lang(self, lang):
        self.lang = lang

    def get_lang(self):
        return self.lang

    def set_problem_user_id(self, problem_user_id):
        self.problem_user_id = problem_user_id

    def get_problem_user_id(self):
        return self.problem_user_id

    def set_problem_theme(self, problem_theme):
        self.problem_theme = problem_theme

    def get_problem_theme(self):
        return self.problem_theme

    def set_problem_exam(self, problem_exam):
        self.problem_exam = problem_exam

    def get_problem_exam(self):
        return self.problem_exam

    def set_problem_argument(self, problem_argument):
        self.problem_argument = problem_argument

    def get_problem_argument(self):
        return self.problem_argument

    def set_problem_solution(self, problem_solution):
        self.problem_solution = problem_solution

    def get_problem_solution(self):
        return self.problem_solution

    def registration_problem(self):
        user_id = self.get_problem_user_id()
        theme = self.get_problem_theme()
        exam = self.get_problem_exam()

        if type(user_id) is int and theme and exam:
            session = DBsession()
            test_table = TestModel()
            test_table.user_id = user_id
            test_table.theme = theme
            test_table.exam = exam
            session.add(test_table)
            session.commit()
            self.set_problem_id(test_table.id)
        else:
            pass

    def add_test_case(self):
        if type(self.get_problem_id()) is int:
            test_case = {}
            test_case["problem_id"] = self.get_problem_id()
            test_case["problem_argument"] = self.get_problem_argument()
            test_case["problem_solution"] = self.get_problem_solution()
            self.problem_test_cases_list.append(test_case)

    def registration_test_cases(self):
        for i in self.problem_test_cases_list:
            print(i)
            try:
                session = DBsession()
                test_cases_table = TestCasesModel()
                test_cases_table.test_id = i["problem_id"]
                test_cases_table.argument = i["problem_argument"]
                test_cases_table.solution = i["problem_solution"]
                session.add(test_cases_table)
                session.commit()
            except BaseException as e:
                print(e)
                session.rollback()
            finally:
                session.close()

    def choice_problem(self, problem_id):
        list_test_case = []
        if type(problem_id) is int:
            session = DBsession()
            list_test_case = session.query(TestCasesModel.id, TestCasesModel.argument, TestCasesModel.solution).filter(
                TestCasesModel.test_id == problem_id).all()
            session.close()
            return list_test_case
        else:
            return list_test_case

# coder = Coder()
# coder.set_problem_id(1)
# coder.set_code("prit()")
# coder.set_lang("lang")
# print(coder.get_code())
# print(coder.get_problem_id())
# print(coder.get_lang())
# coder = Problem()
# coder.set_problem_user_id(1)
# coder.set_problem_theme("testProblem")
# coder.set_problem_exam("testProblem")
# coder.registration_problem()

# coder.set_problem_argument("testProblem_test1")
# coder.set_problem_solution("testProblem_test1")
# coder.add_test_case()

# coder.set_problem_argument("testProblem_test2")
# coder.set_problem_solution("testProblem_test2")
# coder.add_test_case()

# coder.registration_test_cases()


# problem = Problem()
# problem.set_problem_id(26)
# print(problem.choice_problem())


# problem = Problem()
# test_cases = problem.choice_problem(1)

# print(test_cases)
