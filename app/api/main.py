
import hashlib
import json
import os
import random
import time
from datetime import datetime, timedelta


from sqlalchemy import exc
import pika
import tornado
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.autoreload

from datastore.models import *
from datastore.setting import DBsession
from account import Account
from auth import Auth
from problem import Problem
from coder import Coder

RabbitMQ_HOST = "{0}".format(os.environ["RabbitMQ_HOST"])

JWT_KEY = "secret"


def mq(lang, source, argument, code_test_table_id,solution):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RabbitMQ_HOST))
    channel = connection.channel()

    channel.queue_declare(queue='code')

    requestData = {
        "code_test_table_id": code_test_table_id,
        "lang": lang,
        "source": source,
        "argument": argument,
        "solution": solution,
    }
    requestData = json.dumps(requestData)

    channel.basic_publish(exchange='',
                          routing_key='code',
                          body=requestData)
    # print("[Rabbitmq] Sent " + requestData)
    connection.close()

class CodeResultWebSocket(tornado.websocket.WebSocketHandler):
    def open(self, uid):
        self.uid = uid
        self.ioloop = tornado.ioloop.IOLoop.instance()
        self.send_websocket()

    def on_close(self):
        print("Session closed")

    def check_origin(self, origin):
        return True

    def send_websocket(self):
        self.ioloop.add_timeout(
            time.time() + 3, self.send_websocket)  # 3秒に一回更新
        if self.ws_connection:
            session = DBsession()
            code_tests_model = session.query(CodeTestsModel.id, CodeTestsModel.result, CodeTestsModel.judgment).filter(
                CodeTestsModel.id == self.uid).all()
            results = json.dumps(code_tests_model[0])
            self.write_message(results)


class CodeResultHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')
        self.set_header('Access-Control-Allow-Headers',
                        'authorization,content-type')

    def get(self, codeId):
        session = DBsession()
        code_tests_model = session.query(CodeTestsModel.id, CodeTestsModel.result, CodeTestsModel.judgment).filter(
            CodeTestsModel.id == codeId).all()
        results = json.dumps(code_tests_model[0])
        self.write(results)

class CodeReciveHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')
        self.set_header('Access-Control-Allow-Headers',
                        'authorization,content-type')

    def options(self):
        pass

    def post(self):
        data = json.loads(self.request.body)
        auth_header = self.request.headers.get("Authorization")
        account = Account()
        account.set_auth_header(auth_header)
        isSignin = account.auth_header_check()
        if isSignin is False:
            self.write(json.dumps({}))
        else:
            coder = Coder()
            coder.set_user_id(account.get_jwt_auth_id())
            coder.set_problem_id(data["problemid"])
            coder.set_code(data["code"])
            coder.set_lang(data["lang"])
            coder.registration_code()

            problem = Problem()
            test_cases = problem.choice_problem(data["problemid"])
            coder.set_choice_problem(test_cases)
            coder.registration_test_cases()
            # coder.get_code_each_test_cases() 各コードごとのテストコード
            code_test_case_id_list = coder.get_code_each_test_cases()
            print(code_test_case_id_list)
            # rabbitmqへの送信
            for i in code_test_case_id_list:
                mq(lang=coder.get_lang() , source=coder.get_code(),argument=i[1], code_test_table_id=i[0],solution=i[2])

            response_data = json.dumps([i[0] for i in code_test_case_id_list])
            self.write(response_data)
   
class ResultReciveHandler(tornado.web.RequestHandler):
    def post(self):
        # クライアントからのアクセスはないハズ
        # 認証キーを発行するといいかもしれない
        # コードの結果(json)->DB
        data = json.loads(self.request.body)
        coder = Coder()
        coder.set_code_test_table_id(data["code_test_table_id"])
        coder.get_code_test_id_to_test_case()
        coder.set_results(data["result"])
        coder.set_judgment(data["judge"])
        coder.check_solution()
        self.write("test")


class ManagementTestHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')
        self.set_header('Access-Control-Allow-Headers',
                        'authorization,content-type')

    def options(self):
        print(self.request.headers)
        pass

    def post(self):  # new test case
        auth_header = self.request.headers.get("Authorization")
        account = Account()
        account.set_auth_header(auth_header)
        isSignin = account.auth_header_check()
        print("user id: {0}".format(account.get_jwt_auth_id()))

        if isSignin is True:
            data = json.loads(self.request.body)
            problem = Problem()
            problem.set_problem_user_id(account.get_jwt_auth_id())
            problem.set_problem_theme(data["theme"])
            problem.set_problem_exam(data["exam"])
            problem.registration_problem()

            for i in data["test_cases"]:
                problem.set_problem_argument(i["argument"])
                problem.set_problem_solution(i["solution"])
                problem.add_test_case()

            problem.registration_test_cases()
            self.write(json.dumps({"SigIn": isSignin}))
        else:
            self.write(json.dumps({"SigIn": isSignin}))


class ProblemDescriptionHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')

    def options(self):
        print(self.request.headers)
        pass

    def get(self, problem_number_param):
        response_data = {}
        if problem_number_param.isdecimal():
            problem_number_param = int(problem_number_param)
            session = DBsession()
            getAll = session.query(TestModel.id, TestModel.theme, TestModel.exam).filter(
                TestModel.id == problem_number_param).all()

            if len(getAll) == 1:  # idから引くので一件だけ引っかかるはず
                problem = getAll[0]
                response_data = {
                    "id": problem[0],
                    "theme": problem[1],
                    "exam": problem[2]
                }
            else:
                # ログにしたほうがいい
                pass
        response_data = json.dumps(response_data)
        self.write(response_data)


class ProblemsListHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')

    def options(self):
        pass

    def get(self):
        # databaseへの処理
        session = DBsession()
        getAll = session.query(TestModel.id, TestModel.theme).all()

        problemList = []
        for i in getAll:
            print({"id": i[0], "theme": i[1]})
            problemList.append({"id": i[0], "theme": i[1]})

        problemList = json.dumps(problemList)
        self.write(problemList)


class AccountDetailHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')

    def options(self):
        pass

    def get(self):
        auth_header = self.request.headers.get("Authorization")
        auth = Auth()
        login_check = auth.auth_header_check(auth_header)
        response_data = json.dumps(login_check)
        self.write(response_data)


class SignUPHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')

    def options(self):
        pass

    def post(self):  # new test case
        data = json.loads(self.request.body)

        account = Account()
        account.set_email(data["email"])
        account.set_password_1(data["password_1"])
        account.set_password_2(data["password_2"])
        account.set_first_name(data["first_name"])
        account.set_last_name(data["last_name"])

        msg = account.SignUp()
        response_data = json.dumps(msg)
        self.write(response_data)


class SignInHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')

    def options(self):
        pass

    def post(self):
        data = json.loads(self.request.body)
        email = data["email"]
        password = data["password"]

        account = Account()
        account.set_email(email)
        account.set_password(password)
        response_data = account.SigIn()
        response_data = json.dumps(response_data)
        self.write(response_data)


class IDtoNameHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')
        self.set_header('Access-Control-Allow-Headers', 'authorization')

    def options(self):
        pass

    def get(self):
        auth_header = self.request.headers.get("Authorization")
        account = Account()
        account.set_auth_header(auth_header)
        isSignin = account.auth_header_check()
        if isSignin == True:
            account.get_id_to_name()
            account.get_last_name()
            full_name = {}
            full_name["first_name"] = account.get_first_name()
            full_name["last_name"] = account.get_last_name()
            response_data = json.dumps(full_name)
            self.write(response_data)
        else:
            response_data = json.dumps({"msg": "not sig in"})
            self.write(response_data)


class TokenValidateHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header('Access-Control-Max-Age', 1000)
        self.set_header('Content-type', 'application/json')
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')
        self.set_header('Access-Control-Allow-Headers',
                        'Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Headers, X-Requested-By, Access-Control-Allow-Methods')

    def options(self):
        pass

    def post(self):
        print(self.request.headers)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        print("tews")
        self.write("test_")


class Health_CheckHandler(tornado.web.RequestHandler):
    def get(self):
        auth_header = self.request.headers.get("Authorization")
        print(auth_header)
        self.write("ok")


app = tornado.web.Application([
    (r"/code/result/display/(.+)", CodeResultWebSocket),
    (r"/code/result/(.+)", CodeResultHandler),
    (r"/", MainHandler),
    (r"/code", CodeReciveHandler),
    (r"/code/results", ResultReciveHandler),
    (r"/problems", ManagementTestHandler),
    (r"/problems/id/(.+)", ProblemDescriptionHandler),
    (r"/problems/list", ProblemsListHandler),
    (r"/auth/signup", SignUPHandler),
    (r"/auth/signin", SignInHandler),
    (r"/auth/token", TokenValidateHandler),
    (r"/account/name", IDtoNameHandler),
    (r"/account", AccountDetailHandler),
    (r"/health", Health_CheckHandler),
], debug=True, autoreload=True)

if __name__ == "__main__":
    app.listen(8080)
    instance = tornado.ioloop.IOLoop.instance()
    instance.start()
