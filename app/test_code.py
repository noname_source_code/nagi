import unittest
from api.account import *


class Account_Test(unittest.TestCase):

    def setUp(self):
        self.account = Account()

    def test_password_to_hash_password(self):
        actual = self.account.password_to_hash_password("test_1")
        expect = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6f00a08"
        self.assertEqual(expect, actual)


if __name__ == "__main__":
    unittest.main(verbosity=2)
