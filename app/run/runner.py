import os
import docker
import json
import requests


class Runner(object):
    def __init__(self):
        self.lang = None
        self.code_test_table_id = None
        self.source = None
        self.argument = None
        self.extension = None
        self.cmd = None
        self.bind_source_file = None
        self.path = None
        self.result = None

    def set_data(self, json_data):
        code_test_table_id_key = "code_test_table_id" in json_data
        lang_key = "lang" in json_data
        source_key = "source" in json_data
        argument_key = "argument" in json_data
        if code_test_table_id_key and lang_key and source_key and argument_key:
            self.code_test_table_id = json_data["code_test_table_id"]
            self.lang = json_data["lang"]
            self.source = json_data["source"]
            self.argument = json_data["argument"]

    def get_extension(self):
        return self.extension

    def get_cmd(self):
        return self.cmd

    def get_bind_source_file(self):
        # コンテナ内のどこにバインドするかのパス
        # コンテン内で使えるディレクトリパスが入っている
        '''
        # コンテナ
        '''
        return self.bind_source_file

    def get_code_test_table_id(self):
        return self.code_test_table_id

    def get_source(self):
        return self.source

    def get_path(self):
        # ホスト場所
        '''
        # ホスト
        '''
        return self.path

    def make_file(self):
        code_test_table_id = self.get_code_test_table_id()
        extension = self.get_extension()
        source = self.get_source()
        self.path = os.getcwd() + '/code/code-' + \
            str(code_test_table_id) + extension
        with open(self.path, mode='w') as f:
            f.write(source)

    def set_python(self):
        self.extension = ".py"
        self.cmd = "bash /starter/code-start.sh python3 /source/main.py"
        split_arg = self.argument.splitlines()
        for i in split_arg:
            self.cmd += " " + i
        self.bind_source_file = '/source/main.py'

    def run_code(self):
        client = docker.from_env()
        cmd = self.get_cmd()
        code_test_table_id = self.get_code_test_table_id()
        path = self.get_path()
        bind_source_file = self.get_bind_source_file()
        try:
            result = client.containers.run(
                "dev-alpine",
                cmd,
                volumes={path: {'bind': bind_source_file, 'mode': 'rw'}},
                remove=True
            )
            self.result = result.decode('utf-8')
        except:
            self.result = "Code: Err"

            pass

    def send_result_api_server(self):
        code_test_table_id = self.code_test_table_id
        result = self.result
        url = "http://localhost:8080/code/results"
        data = json.dumps({
            "code_test_table_id": code_test_table_id,
            "result": result,
        })
        header = {'content-type': 'application/json'}
        requests.post(url, data=data, headers=header)

# code_test_table_id = data["code_test_table_id"]
# test_case_id = data["test_case"]["test_cases_id"]
# code_result_request(code_test_table_id,
#                     result.decode('utf-8'), test_case_id)
# except:
#     result = str(data["lang"]) + ": Code Error"
#     code_test_table_id = data["code_test_table_id"]
#     test_case_id = data["test_case"]["test_cases_id"]
#     code_result_request(code_test_table_id,
#                         result, test_case_id)
