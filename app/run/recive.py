import pika
import time
import json
import docker
import rethinkdb
import os
import requests

from runner import Runner

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue="code")


def callback(ch, method, properties, body):
    data = json.loads(body.decode('utf-8'))

    runner = Runner()
    runner.set_data(data)
    runner.set_python()
    # print(runner.get_cmd())
    # print(runner.get_bind_source_file())
    # print(runner.get_extension())
    runner.make_file()
    runner.run_code()
    runner.send_result_api_server()

    # extension, cmd, bind_source_file = code_handler(data)

    # path = os.getcwd() + '/code/code-' + \
    #     str(data["code_test_table_id"]) + extension
    # with open(path, mode='w') as f:
    #     f.write(data["source"])

    # # 引数の設定方法 言語,ソースコードの場所 言語に与える入力
    # client = docker.from_env()
    # try:
    #     result = client.containers.run("dev-alpine", cmd, volumes={
    #         path: {'bind': bind_source_file, 'mode': 'rw'}}, remove=True)
    #     code_test_table_id = data["code_test_table_id"]
    #     test_case_id = data["test_case"]["test_cases_id"]
    #     code_result_request(code_test_table_id,
    #                         result.decode('utf-8'), test_case_id)
    # except:
    #     result = str(data["lang"]) + ": Code Error"
    #     code_test_table_id = data["code_test_table_id"]
    #     test_case_id = data["test_case"]["test_cases_id"]
    #     code_result_request(code_test_table_id,
    #                         result, test_case_id)


def code_result_request(code_test_table_id, result, test_cases_id):
    url = "http://localhost:8080/code/results"
    data = json.dumps({
        "test_case_table_id": test_cases_id,
        "code_test_table_id": code_test_table_id,
        "result": result,
    })
    header = {'content-type': 'application/json'}
    requests.post(url, data=data, headers=header)


def code_handler(data):
    lang = data["lang"]
    if lang == 'python':
        extension = ".py"
        cmd = "bash /starter/code-start.sh python3 /source/main.py"
        for i in data["test_case"]["argument"].splitlines():
            cmd += " " + i
        bind_source_file = '/source/main.py'
    elif lang == 'Clang':
        extension = ".c"
        cmd = "bash /starter/code-start.sh Clang /source/main.c"
        for i in data["test_case"]["argument"].splitlines():
            cmd += " " + i

        bind_source_file = '/source/main.c'
    print(extension)
    print(cmd)
    print(bind_source_file)

    return extension, cmd, bind_source_file


channel.basic_consume(queue='code',
                      auto_ack=True,
                      on_message_callback=callback)
channel.start_consuming()
