FROM python:3

WORKDIR /usr/src/app

COPY ./requirements.txt ./
COPY ./app/api ./api
RUN pip install --no-cache-dir -r requirements.txt
CMD [ "python", "-u", "./api/main.py" ]